public class BlockExample{
// static variable
static int j = 10;
static int n;
 
// static block
static {
System.debug("Static block initialized.");
n = j * 8;
}
 
public static void main()
{
System.debug("Inside main method");
System.debug("Value of j : "+j);
System.debug("Value of n : "+n);
}
}

