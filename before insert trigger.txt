trigger AccountTrigger on Account (before insert) {
    List<Account> newaccountList = trigger.new;
    
    for( Account acc : newaccountList){
        acc.AnnualRevenue = 0.9 * acc.AnnualRevenue;
    }
}