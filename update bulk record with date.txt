List<Student__c> studentList = [SELECT Id, Name, Roll_No__c, Annual_Fees__c, Teacher__c 
                                FROM Student__c];

for(Student__c studentRecord : studentList){
    studentRecord.Admission_Date__c = Date.today();
}
UPDATE studentList;