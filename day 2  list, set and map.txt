Map<String, String> colorMap = new Map<String, String>();

colorMap.put('Pranil','Red');
colorMap.put('Pari','Yellow');
colorMap.put('Aman','Orange');
colorMap.put('Ajay','Green');
colorMap.put('Rafeya','Black');
colorMap.put('John','Orange');

System.debug('colorMap:::'+colorMap);
System.debug('colorMap.keySet():::'+colorMap.keySet());
for( String name : colorMap.keySet() ){
    System.debug('name:::'+name + ' color:::'+colorMap.get(name));
}


List<String> nameList = new List<String>();
nameList.add('amol');
nameList.add('ajay');
nameList.add('sari');
nameList.add('Pranil');
nameList.add('Aman');
System.debug('nameList:::'+nameList);

for(String name : nameList){
    System.debug('name:::'+name);
}

Set<String> nameSet = new Set<String>();
nameSet.add(null);
nameSet.add(null);
nameSet.add(null);
nameSet.add('Pranil');
nameSet.add('Aman');
System.debug('nameSet:::'+nameSet);